# Sortiert Video- und Bilddateien,
# die in ihrem Namen das Entstehungsdatum im Format jjjjmmtt, z.B. 20190822, haben,
# nach dem Monat ein.
# Ausgelegt für die verwendung mit der Git-Bash unter Windows.
# 
# Wenn Git for Windows installiert ist, die datei einfach in den entsprechenden Ordner kopieren 
# und dort (z.B. mit Doppelklick) ausführen.

shopt -s extglob

dateien=$(ls | grep -v '/')
gesamtzahl=$(ls | grep -v '/' | wc -l)
aktuelleZahl=1

echo -ne "Kategorisiere $aktuelleZahl/$gesamtzahl"

for datei in $dateien; do 
  
  aktuelleZahl=$((aktuelleZahl+1))
  echo -ne "\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\rKategorisiere $aktuelleZahl/$gesamtzahl"
  
  verzeichnis="${datei##*([^0-9])}"
  verzeichnis="${verzeichnis:0:6}"
  verzeichnis="${verzeichnis:0:4}-${verzeichnis:4:2}"
  
  if [[ $verzeichnis =~ ^[0-9]{4}-[0-9]{2}$ ]]; then
    mkdir -p "$verzeichnis"
    mv "${datei}" "$verzeichnis/$datei"
  fi;
 done
